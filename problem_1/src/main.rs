/*Multiples of 3 of 5
*/
fn multiples_3_or_5(n: u32) -> u32 {
    (1..=n)
        .filter(|i| i % 3 == 0 || i % 5 == 0)
        .fold(0, |total, next| total + next)
}
fn main() {
    println!("{}", multiples_3_or_5(1000));
}
